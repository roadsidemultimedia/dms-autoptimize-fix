<?php
/*
Plugin Name: DMS Autoptimize Fix
Description: Prevents Autoptimize plugin from breaking DMS front-end
Version: 1.0.0
Author: Roadside Multimedia
Contributors: Milo Jennings, Curtis Grant
*/

// Set Autoptimize plugin to skip optimization when logged in
// Otherwise, it breaks the front end editor, and produces a 500 server error.
add_filter('autoptimize_filter_noptimize','dms_ao_noptimize',10,0);
function dms_ao_noptimize() {
    if (is_user_logged_in()) {
        return true;
    } else {
        return false;
    }
}